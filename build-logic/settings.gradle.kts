include("commons")
include("java-library")
include("kotlin-library")

dependencyResolutionManagement {
    repositories {
        gradlePluginPortal()
    }
}
