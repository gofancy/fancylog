plugins {
    `kotlin-dsl`
}

dependencies {
    implementation(project(":commons"))
    implementation(project(":java-library"))

    implementation(kotlin("gradle-plugin", version = "1.6.10"))
}
