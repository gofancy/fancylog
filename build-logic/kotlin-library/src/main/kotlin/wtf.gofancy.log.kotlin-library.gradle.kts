import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("wtf.gofancy.log.commons")
    id("wtf.gofancy.log.java-library")
    kotlin("jvm")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()

        languageVersion = "1.6"
        apiVersion = "1.6"
    }
}
