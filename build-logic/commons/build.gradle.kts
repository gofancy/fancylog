plugins {
    `kotlin-dsl`
}

dependencies {
    implementation(group = "gradle.plugin.fr.brouillard.oss.gradle", name = "gradle-jgitver-plugin", version = "0.6.1")
    implementation(group = "gradle.plugin.org.cadixdev.gradle", name = "licenser", version = "0.6.1")
}
