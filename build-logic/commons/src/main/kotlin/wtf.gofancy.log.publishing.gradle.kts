plugins {
    `maven-publish`
}

publishing {
    repositories {
        maven {
            name = "gitlab"
            url = uri("https://gitlab.com/api/v4/projects/29005781/packages/maven")

            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }

    publications {
        create<MavenPublication>(project.name) {
            from(components["java"])

            pom {
                name.set("FancyLog ${project.name}")
                description.set(project.description)
                inceptionYear.set("2021")
                url.set("https://gitlab.com/gofancy/fancylog")

                organization {
                    name.set("Garden of Fancy")
                    url.set("https://gitlab.com/gofancy")
                }

                licenses {
                    license {
                        name.set("MIT")
                        url.set("https://mit-license.org/")
                    }
                }
            }
        }
    }
}
