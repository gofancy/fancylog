import fr.brouillard.oss.gradle.plugins.JGitverPluginExtensionBranchPolicy
import fr.brouillard.oss.jgitver.Strategies

plugins {
    id("fr.brouillard.oss.gradle.jgitver")
}

jgitver {
    strategy = Strategies.MAVEN

    policy(closureOf<JGitverPluginExtensionBranchPolicy> {
        pattern = "(main)"
        transformations = listOf("IGNORE")
    })
    policy(closureOf<JGitverPluginExtensionBranchPolicy> {
        pattern = "develop/(.*)"
    })
}
