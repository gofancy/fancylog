import java.util.Calendar

plugins {
    id("org.cadixdev.licenser")
}

license {
    header(rootProject.file("NOTICE"))

    ext["year"] = "2021-${Calendar.getInstance().get(Calendar.YEAR)}"
    ext["name"] = "Garden of Fancy"
    ext["app"] = "FancyLog"
}
