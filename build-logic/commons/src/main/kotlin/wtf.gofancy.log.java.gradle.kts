import java.time.Instant
import java.time.format.DateTimeFormatter

plugins {
    java
}

tasks.withType<Jar> {
    manifest {
        attributes(
            mapOf(
                "Specification-Title" to "FancyLog ${project.name}",
                "Specification-Version" to provider { project.version },
                "Specification-Vendor" to "Garden of Fancy",
                "Implementation-Title" to "${project.group}.${project.name.toLowerCase()}",
                "Implementation-Version" to provider { project.version },
                "Implementation-Vendor" to "Garden of Fancy",
                "Implementation-Timestamp" to DateTimeFormatter.ISO_INSTANT.format(Instant.now())
            ), "${project.group.toString().replace(".", "/")}/${project.name}/"
        )
    }
}
