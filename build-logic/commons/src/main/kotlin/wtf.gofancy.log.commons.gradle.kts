import org.gradle.api.internal.artifacts.DefaultModuleVersionIdentifier
import org.gradle.api.publish.maven.internal.publication.DefaultMavenPublication

plugins {
    id("wtf.gofancy.log.java")
    id("wtf.gofancy.log.versioning")
    id("wtf.gofancy.log.headers")
    id("wtf.gofancy.log.publishing")
}

group = "wtf.gofancy.log"

val log: String? by project
if (log?.toBoolean() == true) {
    afterEvaluate {
        val artifact = (publishing.publications[name] as? DefaultMavenPublication)?.let { publication ->
            (publication.coordinates as? DefaultModuleVersionIdentifier)?.toString()
        } ?: "~~UNKNOWN~~"

        logger.lifecycle("************************")
        logger.lifecycle("module '$name' evaluated:")
        logger.lifecycle(description)
        logger.lifecycle("version: '$version'")
        logger.lifecycle("artifact: '$artifact'")
        logger.lifecycle("************************")
    }
}
