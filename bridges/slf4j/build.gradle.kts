plugins {
    id("wtf.gofancy.log.commons")
}

description = """
    An implementation for the Garden of Fancy logger which bridges all logging calls to the SLF4J logging framework.
    Note that an SLF4J binding still needs to be put on the classpath.
""".trimIndent().replace('\n', ' ')

dependencies {
    implementation(group = "wtf.gofancy.log", name = "api")

    implementation(group = "org.slf4j", name = "slf4j-api", version = "2.0.0-alpha4")
}
