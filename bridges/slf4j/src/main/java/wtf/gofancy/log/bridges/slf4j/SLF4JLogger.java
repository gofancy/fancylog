/*
 * This file is part of FancyLog, licensed under the MIT License
 *
 * Copyright (c) 2021-2021 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.log.bridges.slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import wtf.gofancy.log.api.Flog;

import java.util.function.Supplier;

public class SLF4JLogger extends Flog {

    private final Logger logger;
    private final Marker marker;

    public SLF4JLogger(final String name, final String mark) {
        this.logger = LoggerFactory.getLogger(name);
        this.marker = MarkerFactory.getMarker(mark);
    }

    @Override
    public void error(String msg) {
        this.logger.error(this.marker, msg);
    }

    @Override
    public boolean errorEnabled() {
        return this.logger.isErrorEnabled(this.marker);
    }

    @Override
    public void warn(String msg) {
        this.logger.warn(this.marker, msg);
    }

    @Override
    public boolean warnEnabled() {
        return this.logger.isWarnEnabled(this.marker);
    }

    @Override
    public void info(String msg) {
        this.logger.info(this.marker, msg);
    }

    @Override
    public boolean infoEnabled() {
        return this.logger.isInfoEnabled(this.marker);
    }

    @Override
    public void debug(String msg) {
        this.logger.debug(this.marker, msg);
    }

    @Override
    public boolean debugEnabled() {
        return this.logger.isDebugEnabled(this.marker);
    }

    @Override
    public void trace(Supplier<String> msgSup) {
        if (this.logger.isDebugEnabled(this.marker)) {
            this.logger.trace(this.marker, msgSup.get());
        }
    }
}
