plugins {
    id("wtf.gofancy.log.commons")
}

description = """
    An implementation for the Garden of Fancy logger which bridges all logging calls to Apaches Log4j logging framework.
""".trimIndent().replace('\n', ' ')

dependencies {
    implementation(group = "wtf.gofancy.log", name = "api")

    implementation(group = "org.apache.logging.log4j", name = "log4j-api", version = "2.14.1")
    // add core as runtime dependency so that it is available on the classpath
    runtimeOnly(group = "org.apache.logging.log4j", name = "log4j-core", version = "2.14.1")
}
