includeBuild("../build-logic")
includeBuild("../base")

rootProject.name = "bridges"

setOf("log4j", "slf4j").forEach {
    include(it)
    project(":$it").name = "bridge-$it"
}

dependencyResolutionManagement {
    repositories {
        mavenCentral()
    }
}
