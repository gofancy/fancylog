// umbrella build script

plugins {
    java
    `maven-publish`
    id("org.cadixdev.licenser") version "0.6.1"
}

tasks {
    listOf(
        "assemble",
        "build",
        "clean",
        "check",
        "publish",
        "publishToMavenLocal",
        "licenseFormat"
    ).map(::named).forEach {
        it.configure {
            dependOnAllModules()
        }
    }
}

fun Task.dependOnAllModules() {
    project.logger.lifecycle("Task '$name' depends on all modules!")

    dependsOn(
        gradle.includedBuild("base").task(":api:$name"),
        gradle.includedBuild("base").task(":golfer:$name"),
        gradle.includedBuild("bridges").task(":bridge-log4j:$name"),
        gradle.includedBuild("bridges").task(":bridge-slf4j:$name"),
    )
}
