plugins {
    id("wtf.gofancy.log.kotlin-library")
}

description = """
    An basic service implementation which aims to be a plug and play solution for logging. Rather than requiring
    configuration files like other frameworks. Great for quick testing/prototyping.
""".trimIndent().replace('\n', ' ')

dependencies {
    implementation(project(":api"))
    implementation(group = "org.fusesource.jansi", name = "jansi", version = "2.3.4")
}
