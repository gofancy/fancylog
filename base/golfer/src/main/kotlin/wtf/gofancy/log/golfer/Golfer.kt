/*
 * This file is part of FancyLog, licensed under the MIT License
 *
 * Copyright (c) 2021-2021 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.log.golfer

import org.fusesource.jansi.Ansi.Color
import org.fusesource.jansi.Ansi.ansi
import org.fusesource.jansi.AnsiConsole
import org.fusesource.jansi.AnsiPrintStream
import org.fusesource.jansi.io.AnsiOutputStream
import wtf.gofancy.log.api.Flog
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.function.Supplier

class Golfer(private val name: String, private val mark: String) : Flog() {

    private enum class Level(val displayName: String, val color: Color, val printer: AnsiPrintStream) {
        TRACE("TRACE", Color.WHITE, AnsiConsole.out()),
        DEBUG("DEBUG", Color.MAGENTA, AnsiConsole.out()),
        INFO("INFO", Color.GREEN, AnsiConsole.out()),
        WARN("WARN", Color.YELLOW, AnsiConsole.out()),
        ERROR("ERROR", Color.RED, AnsiConsole.err()),
    }

    private companion object {
        const val INFO_TEMPLATE = "[%s] [%s/%s] [%s/%s]: %s"

        init {
            System.setProperty(AnsiConsole.JANSI_MODE, AnsiConsole.JANSI_MODE_FORCE)
        }
    }

    override fun error(msg: String) {
        log(msg, Level.ERROR)
    }

    override fun errorEnabled() = true

    override fun warn(msg: String) {
        log(msg, Level.WARN)
    }

    override fun warnEnabled() = true

    override fun info(msg: String) {
        log(msg, Level.INFO)
    }

    override fun infoEnabled() = true

    override fun debug(msg: String) {
        log(msg, Level.DEBUG)
    }

    override fun debugEnabled() = true

    override fun trace(msgSup: Supplier<String>?) {
        TODO("Not yet implemented")
    }

    private fun log(msg: String, level: Level) {
        level.printer.println(msg.prependInfo(level).colorize(level.color))
    }

    private fun String.prependInfo(level: Level): String = INFO_TEMPLATE.format(
        DateTimeFormatter.ofPattern("HH:mm:ss.SSSSSS").format(LocalDateTime.now()),
        Thread.currentThread().name,
        level.displayName,
        this@Golfer.name,
        this@Golfer.mark,
        this,
    )

    private fun String.colorize(color: Color): String = ansi().fg(color).a(this).reset().toString()
}
