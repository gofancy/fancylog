includeBuild("../build-logic")

rootProject.name = "base"

include("api")
include("golfer")

dependencyResolutionManagement {
    repositories {
        mavenCentral()
    }
}
