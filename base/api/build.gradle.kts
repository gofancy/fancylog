plugins {
    id("wtf.gofancy.log.java-library")
}

description = """
The base package for the Garden of Fancy Logger.
Contains the API to create new loggers, the SPI and the service loader.
Also ships with a default NO-OP logger in case no other bridges can be found.
""".trimIndent().replace('\n', ' ')
