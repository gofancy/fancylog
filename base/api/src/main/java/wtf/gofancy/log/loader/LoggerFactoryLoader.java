/*
 * This file is part of FancyLog, licensed under the MIT License
 *
 * Copyright (c) 2021-2021 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.log.loader;

import wtf.gofancy.log.api.Flog;
import wtf.gofancy.log.noop.NoOpLoggerFactory;
import wtf.gofancy.log.spi.LoggerFactory;

import java.util.Iterator;
import java.util.ServiceLoader;

// TODO cleaner initialization states and more debug information
public class LoggerFactoryLoader {

    public static final LoggerFactory active;

    private static InitializationState state;

    static {
        active = findFactory();

        if (state.usesNoOp) {
            System.err.println("FancyLog: Encountered error state during initialization.");
            System.err.println("FancyLog: Please see the message below for more information.");
            System.err.println("FancyLog: Error: " + state.statusMessage);
        } else {
            final Flog log = active.create("FancyLogger", "Initialization");

            // TODO use formats
            log.info("Using factory '" + active.name() +"'");
            log.info("Status: " + state.statusMessage);
        }
    }

    private static LoggerFactory findFactory() {
        /*
         * We use the class loader of this class and not the threads one to make it compatible with Minecraft Forge.
         * There is no other reason behind it. Though it should not affect most applications.
         */
        final ServiceLoader<LoggerFactory> loader = ServiceLoader.load(LoggerFactory.class, LoggerFactoryLoader.class.getClassLoader());
        final Iterator<LoggerFactory> services = loader.iterator();

        if (services.hasNext()) {
            final LoggerFactory factory = services.next();
            state = services.hasNext() ? InitializationState.MULTIPLE_FACTORIES : InitializationState.SUCCESSFUL;
            return factory;
        } else {
            state = InitializationState.ZERO_FACTORIES;
            return NoOpLoggerFactory.INSTANCE;
        }
    }

    private enum InitializationState {
        SUCCESSFUL(false, "Initiated successful."),
        ZERO_FACTORIES(true, "Found no factories. Falling back to NoOpLogger (no messages will be logged)."),
        MULTIPLE_FACTORIES(false, "Found multiple factories. Chose the first available one.");

        public final boolean usesNoOp;
        public final String statusMessage;

        InitializationState(final boolean usesNoOp, final String statusMessage) {
            this.usesNoOp = usesNoOp;
            this.statusMessage = statusMessage;
        }
    }
}
