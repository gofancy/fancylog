/*
 * This file is part of FancyLog, licensed under the MIT License
 *
 * Copyright (c) 2021-2021 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.log.api.util;

import wtf.gofancy.log.api.DumpStackBehavior;
import wtf.gofancy.log.api.ExceptionInfoBehavior;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Util {

    private static final String CAUGHT_HEADER_TEMPLATE = "Log exception in thread \"%s\" %s ";
    private static final String LOG_HEADER_TEMPLATE = "Log in thread \"%s\": ";
    private static final String AT = "\tat ";

    public static void log(final Consumer<String> logger,
                           /*nullable*/ final String message,
                           final Throwable throwable,
                           final ExceptionInfoBehavior eib) {
        final String stackTraceHeader = String.format(CAUGHT_HEADER_TEMPLATE, Thread.currentThread()
                .getName(), throwable);

        switch (eib) {
            case MESSAGE_ONLY:
                logBlock(logger, message, stackTraceHeader, new StackTraceElement[0]);
                break;
            case NO_CAUSES:
                logBlock(logger, message, stackTraceHeader, throwable.getStackTrace());
                break;
            case FULL_TRACE:
                logBlock(logger, message, stackTraceHeader, ExceptionUtil.gatherStackTrace(throwable));
                break;
        }
    }

    public static void log(final Consumer<String> logger,
                           /*nullable*/ final String message,
                           final DumpStackBehavior dsb) {
        final String stackTraceHeader = String.format(LOG_HEADER_TEMPLATE, Thread.currentThread().getName());
        final StackTraceElement[] stackTrace = StackTraceUtil.gatherStackTrace(dsb);

        logBlock(logger, message, stackTraceHeader, stackTrace);
    }

    public static void logBlock(final Consumer<String> logger,
                                /*nullable*/ final String message,
                                /*nullable*/ final String stackTraceHeader,
                                final StackTraceElement[] stackTrace) {
        logBlock(logger, message, stackTraceHeader, formatStackTrace(stackTrace));
    }

    private static void logBlock(final Consumer<String> logger,
                                 /*nullable*/ final String message,
                                 /*nullable*/ final String stackTraceHeader,
                                 /*nullable*/ final String stackTrace) {
        final String fullMessage = buildFullMessage(message, stackTraceHeader, stackTrace);
        logBlock(logger, fullMessage);
    }

    public static void logBlock(Consumer<String> logger, String message) {
        if (message.isEmpty()) return;
        message = message.replace("\t", "  ");

        final int longestMessageLength = Arrays.stream(message.split("\n"))
                .mapToInt(String::length)
                .max()
                .orElseThrow(IllegalStateException::new);

        logger.accept(repeatString("*", longestMessageLength + 4));

        Arrays.stream(message.split("\n")).forEach(line -> {
            final int spaces = longestMessageLength - line.length();
            logger.accept("* " + line + repeatString(" ", spaces) + " *");
        });

        logger.accept(repeatString("*", longestMessageLength + 4));
    }

    private static String buildFullMessage(/*nullable*/ final String message,
                                           /*nullable*/ final String stackTraceHeader,
                                           /*nullable*/ final String stackTrace) {
        final StringBuilder builder = new StringBuilder();

        if (message != null && !message.isEmpty()) {
            builder.append(message).append("\n\n");
        }

        if (stackTrace != null && !stackTrace.isEmpty()) {
            // header is only required if the stack trace itself gets logged
            if (stackTraceHeader != null && !stackTraceHeader.isEmpty()) {
                builder.append(stackTraceHeader).append("\n");
            }
            builder.append(stackTrace);
        }

        // strip to remove leading or trailing new lines
        return builder.toString().replaceAll("^[ \t]+|[ \t]+$", "");
    }

    private static /*nullable*/ String formatStackTrace(final StackTraceElement[] trace) {
        if (trace == null || trace.length == 0) {
            return null;
        }
        return Arrays.stream(trace).map(StackTraceElement::toString).collect(Collectors.joining("\n", AT, ""));
    }

    // UTILS >>

    /**
     * Quick little hack to backport j11 String#repeat
     * @param origin the string to repeat
     * @param count how often it should be repeated
     * @return the origin appended count times
     */
    private static String repeatString(final String origin, final int count) {
        return new String(new char[count]).replace("\0", origin);
    }
}
