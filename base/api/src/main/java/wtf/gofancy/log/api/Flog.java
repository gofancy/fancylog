/*
 * This file is part of FancyLog, licensed under the MIT License
 *
 * Copyright (c) 2021-2021 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.log.api;

import wtf.gofancy.log.api.util.Util;
import wtf.gofancy.log.loader.LoggerFactoryLoader;

import java.util.function.Supplier;

public abstract class Flog {

    public static Flog of(final String name, final String mark) {
        return LoggerFactoryLoader.active.create(name, mark);
    }

    //<editor-fold desc="error">
    public abstract void error(final String msg);

    public abstract boolean errorEnabled();

    public void error(final String format, final Object... args) {
        if (this.errorEnabled()) {
            final String msg = String.format(format, args);
            this.error(msg);
        }
    }

    public void errorBlock(final String msg) {
        if (this.errorEnabled()) {
            Util.log(this::error, msg, DumpStackBehavior.NO_DUMP);
        }
    }

    public void errorBlock(final String format, final Object... args) {
        if (this.errorEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::error, msg, DumpStackBehavior.NO_DUMP);
        }
    }

    public void errorBlock(final String msg, final DumpStackBehavior dsb) {
        if (this.errorEnabled()) {
            Util.log(this::error, msg, dsb);
        }
    }

    public void errorBlock(final String format, final DumpStackBehavior dsb, final Object... args) {
        if (this.errorEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::error, msg, dsb);
        }
    }

    public void errorException(final String msg, final Throwable t) {
        if (this.errorEnabled()) {
            Util.log(this::error, msg, t, ExceptionInfoBehavior.FULL_TRACE);
        }
    }

    public void errorException(final String format, final Throwable t, final Object... args) {
        if (this.errorEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::error, msg, t, ExceptionInfoBehavior.FULL_TRACE);
        }
    }

    public void errorException(final String msg, final Throwable t, final ExceptionInfoBehavior eib) {
        if (this.errorEnabled()) {
            Util.log(this::error, msg, t, eib);
        }
    }

    public void errorException(final String format,
                               final Throwable t,
                               final ExceptionInfoBehavior eib,
                               final Object... args) {
        if (this.errorEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::error, msg, t, eib);
        }
    }

    public void errorException(final String msg,
                               final String hint,
                               final String fix,
                               final Throwable t,
                               final ExceptionInfoBehavior eib) {
        // TODO implement properly
        if (this.errorEnabled()) {
            final String finalMsg = String.format("%s\n\nhint: %s\npossible fix: %s", msg, hint, fix);
            Util.log(this::error, finalMsg, t, eib);
        }
    }
    //</editor-fold>

    //<editor-fold desc="warn">
    public abstract void warn(final String msg);

    public abstract boolean warnEnabled();

    public void warn(final String format, final Object... args) {
        if (this.warnEnabled()) {
            final String msg = String.format(format, args);
            this.warn(msg);
        }
    }

    public void warnBlock(final String msg) {
        if (this.warnEnabled()) {
            Util.log(this::warn, msg, DumpStackBehavior.NO_DUMP);
        }
    }

    public void warnBlock(final String format, final Object... args) {
        if (this.warnEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::warn, msg, DumpStackBehavior.NO_DUMP);
        }
    }

    public void warnBlock(final String msg, final DumpStackBehavior dsb) {
        if (this.warnEnabled()) {
            Util.log(this::warn, msg, dsb);
        }
    }

    public void warnBlock(final String format, final DumpStackBehavior dsb, final Object... args) {
        if (this.warnEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::warn, msg, dsb);
        }
    }

    public void warnException(final String msg, final Throwable t) {
        if (this.warnEnabled()) {
            Util.log(this::warn, msg, t, ExceptionInfoBehavior.FULL_TRACE);
        }
    }

    public void warnException(final String format, final Throwable t, final Object... args) {
        if (this.warnEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::warn, msg, t, ExceptionInfoBehavior.FULL_TRACE);
        }
    }

    public void warnException(final String msg, final Throwable t, final ExceptionInfoBehavior eib) {
        if (this.warnEnabled()) {
            Util.log(this::warn, msg, t, eib);
        }
    }

    public void warnException(final String format,
                              final Throwable t,
                              final ExceptionInfoBehavior eib,
                              final Object... args) {
        if (this.warnEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::warn, msg, t, eib);
        }
    }

    public void warnException(final String msg,
                              final String hint,
                              final String fix,
                              final Throwable t,
                              final ExceptionInfoBehavior eib) {
        // TODO implement properly
        if (this.warnEnabled()) {
            final String finalMsg = String.format("%s\n\nhint: %s\npossible fix: %s", msg, hint, fix);
            Util.log(this::warn, finalMsg, t, eib);
        }
    }
    //</editor-fold>

    //<editor-fold desc="info">
    public abstract void info(final String msg);

    public abstract boolean infoEnabled();

    public void info(final String format, final Object... args) {
        if (this.infoEnabled()) {
            final String msg = String.format(format, args);
            this.info(msg);
        }
    }

    public void infoBlock(final String msg) {
        if (this.infoEnabled()) {
            Util.log(this::info, msg, DumpStackBehavior.NO_DUMP);
        }
    }

    public void infoBlock(final String format, final Object... args) {
        if (this.infoEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::info, msg, DumpStackBehavior.NO_DUMP);
        }
    }

    public void infoBlock(final String msg, final DumpStackBehavior dsb) {
        if (this.infoEnabled()) {
            Util.log(this::info, msg, dsb);
        }
    }

    public void infoBlock(final String format, final DumpStackBehavior dsb, final Object... args) {
        if (this.infoEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::info, msg, dsb);
        }
    }

    public void infoException(final String msg, final Throwable t) {
        if (this.infoEnabled()) {
            Util.log(this::info, msg, t, ExceptionInfoBehavior.FULL_TRACE);
        }
    }

    public void infoException(final String format, final Throwable t, final Object... args) {
        if (this.infoEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::info, msg, t, ExceptionInfoBehavior.FULL_TRACE);
        }
    }

    public void infoException(final String msg, final Throwable t, final ExceptionInfoBehavior eib) {
        if (this.infoEnabled()) {
            Util.log(this::info, msg, t, eib);
        }
    }

    public void infoException(final String format,
                              final Throwable t,
                              final ExceptionInfoBehavior eib,
                              final Object... args) {
        if (this.infoEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::info, msg, t, eib);
        }
    }

    public void infoException(final String msg,
                              final String hint,
                              final String fix,
                              final Throwable t,
                              final ExceptionInfoBehavior eib) {
        // TODO implement properly
        if (this.infoEnabled()) {
            final String finalMsg = String.format("%s\n\nhint: %s\npossible fix: %s", msg, hint, fix);
            Util.log(this::info, finalMsg, t, eib);
        }
    }
    //</editor-fold>

    //<editor-fold desc="debug">
    public abstract void debug(final String msg);

    public abstract boolean debugEnabled();

    public void debug(final String format, final Object... args) {
        if (this.debugEnabled()) {
            final String msg = String.format(format, args);
            this.debug(msg);
        }
    }

    public void debugBlock(final String msg) {
        if (this.debugEnabled()) {
            Util.log(this::debug, msg, DumpStackBehavior.NO_DUMP);
        }
    }

    public void debugBlock(final String format, final Object... args) {
        if (this.debugEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::debug, msg, DumpStackBehavior.NO_DUMP);
        }
    }

    public void debugBlock(final String msg, final DumpStackBehavior dsb) {
        if (this.debugEnabled()) {
            Util.log(this::debug, msg, dsb);
        }
    }

    public void debugBlock(final String format, final DumpStackBehavior dsb, final Object... args) {
        if (this.debugEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::debug, msg, dsb);
        }
    }

    public void debugException(final String msg, final Throwable t) {
        if (this.debugEnabled()) {
            Util.log(this::debug, msg, t, ExceptionInfoBehavior.FULL_TRACE);
        }
    }

    public void debugException(final String format, final Throwable t, final Object... args) {
        if (this.debugEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::debug, msg, t, ExceptionInfoBehavior.FULL_TRACE);
        }
    }

    public void debugException(final String msg, final Throwable t, final ExceptionInfoBehavior eib) {
        if (this.debugEnabled()) {
            Util.log(this::debug, msg, t, eib);
        }
    }

    public void debugException(final String format,
                               final Throwable t,
                               final ExceptionInfoBehavior eib,
                               final Object... args) {
        if (this.debugEnabled()) {
            final String msg = String.format(format, args);
            Util.log(this::debug, msg, t, eib);
        }
    }

    public void debugException(final String msg,
                               final String hint,
                               final String fix,
                               final Throwable t,
                               final ExceptionInfoBehavior eib) {
        // TODO implement properly
        if (this.debugEnabled()) {
            final String finalMsg = String.format("%s\n\nhint: %s\npossible fix: %s", msg, hint, fix);
            Util.log(this::debug, finalMsg, t, eib);
        }
    }
    //</editor-fold>

    //<editor-fold desc="trace">
    public abstract void trace(final Supplier<String> msgSup);

    // TODO trace methods
    //</editor-fold>

    //<editor-fold desc="abbreviations">
    //<editor-fold desc="error">
    public final void e(final String msg) {
        this.error(msg);
    }

    public final void e(final String msg, final Object... args) {
        this.error(msg, args);
    }

    public final void eb(final String msg) {
        this.errorBlock(msg);
    }

    public final void eb(final String msg, final Object... args) {
        this.errorBlock(msg, args);
    }

    public final void eb(final String msg, final DumpStackBehavior dsb) {
        this.errorBlock(msg, dsb);
    }

    public final void eb(final String msg, final DumpStackBehavior dsb, final Object... args) {
        this.errorBlock(msg, dsb, args);
    }

    public final void ex(final String msg, final Throwable t) {
        this.errorException(msg, t);
    }

    public final void ex(final String msg, final Throwable t, final Object... args) {
        this.errorException(msg, t, args);
    }

    public final void ex(final String msg, final Throwable t, final ExceptionInfoBehavior eib) {
        this.errorException(msg, t, eib);
    }

    public final void ex(final String msg, final Throwable t, final ExceptionInfoBehavior eib, final Object... args) {
        this.errorException(msg, t, eib, args);
    }

    public final void ex(final String msg,
                         final String hint,
                         final String fix,
                         final Throwable t,
                         final ExceptionInfoBehavior eib) {
        this.errorException(msg, hint, fix, t, eib);
    }

    //</editor-fold>
    //<editor-fold desc="warn">
    public final void w(final String msg) {
        this.warn(msg);
    }

    public final void w(final String msg, final Object... args) {
        this.warn(msg, args);
    }

    public final void wb(final String msg) {
        this.warnBlock(msg);
    }

    public final void wb(final String msg, final Object... args) {
        this.warnBlock(msg, args);
    }

    public final void wb(final String msg, final DumpStackBehavior dsb) {
        this.warnBlock(msg, dsb);
    }

    public final void wb(final String msg, final DumpStackBehavior dsb, final Object... args) {
        this.warnBlock(msg, dsb, args);
    }

    public final void wx(final String msg, final Throwable t) {
        this.warnException(msg, t);
    }

    public final void wx(final String msg, final Throwable t, final Object... args) {
        this.warnException(msg, t, args);
    }

    public final void wx(final String msg, final Throwable t, final ExceptionInfoBehavior eib) {
        this.warnException(msg, t, eib);
    }

    public final void wx(final String msg, final Throwable t, final ExceptionInfoBehavior eib, final Object... args) {
        this.warnException(msg, t, eib, args);
    }

    public final void wx(final String msg,
                         final String hint,
                         final String fix,
                         final Throwable t,
                         final ExceptionInfoBehavior eib) {
        this.warnException(msg, hint, fix, t, eib);
    }

    //</editor-fold>
    //<editor-fold desc="info">
    public final void i(final String msg) {
        this.info(msg);
    }

    public final void i(final String msg, final Object... args) {
        this.info(msg, args);
    }

    public final void ib(final String msg) {
        this.infoBlock(msg);
    }

    public final void ib(final String msg, final Object... args) {
        this.infoBlock(msg, args);
    }

    public final void ib(final String msg, final DumpStackBehavior dsb) {
        this.infoBlock(msg, dsb);
    }

    public final void ib(final String msg, final DumpStackBehavior dsb, final Object... args) {
        this.infoBlock(msg, dsb, args);
    }

    public final void ix(final String msg, final Throwable t) {
        this.infoException(msg, t);
    }

    public final void ix(final String msg, final Throwable t, final Object... args) {
        this.infoException(msg, t, args);
    }

    public final void ix(final String msg, final Throwable t, final ExceptionInfoBehavior eib) {
        this.infoException(msg, t, eib);
    }

    public final void ix(final String msg, final Throwable t, final ExceptionInfoBehavior eib, final Object... args) {
        this.infoException(msg, t, eib, args);
    }

    public final void ix(final String msg,
                         final String hint,
                         final String fix,
                         final Throwable t,
                         final ExceptionInfoBehavior eib) {
        this.infoException(msg, hint, fix, t, eib);
    }

    //</editor-fold>
    //<editor-fold desc="debug">
    public final void d(final String msg) {
        this.debug(msg);
    }

    public final void d(final String msg, final Object... args) {
        this.debug(msg, args);
    }

    public final void db(final String msg) {
        this.debugBlock(msg);
    }

    public final void db(final String msg, final Object... args) {
        this.debugBlock(msg, args);
    }

    public final void db(final String msg, final DumpStackBehavior dsb) {
        this.debugBlock(msg, dsb);
    }

    public final void db(final String msg, final DumpStackBehavior dsb, final Object... args) {
        this.debugBlock(msg, dsb, args);
    }

    public final void dx(final String msg, final Throwable t) {
        this.debugException(msg, t);
    }

    public final void dx(final String msg, final Throwable t, final Object... args) {
        this.debugException(msg, t, args);
    }

    public final void dx(final String msg, final Throwable t, final ExceptionInfoBehavior eib) {
        this.debugException(msg, t, eib);
    }

    public final void dx(final String msg, final Throwable t, final ExceptionInfoBehavior eib, final Object... args) {
        this.debugException(msg, t, eib, args);
    }

    public final void dx(final String msg,
                         final String hint,
                         final String fix,
                         final Throwable t,
                         final ExceptionInfoBehavior eib) {
        this.debugException(msg, hint, fix, t, eib);
    }
    //</editor-fold>
    //</editor-fold>
}
